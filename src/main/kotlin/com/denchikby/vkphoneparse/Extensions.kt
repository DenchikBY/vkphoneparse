package com.denchikby.vkphoneparse

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

fun <T> ternary(expression: Boolean, out1: T?, out2: T?): T {
    return if (expression) {
        out1
    } else {
        out2
    } as T
}

inline fun <reified T> genericType() = object : TypeToken<T>() {}.type

inline fun <reified T> Gson.fromJson(json: String) = this.fromJson<T>(json, object : TypeToken<T>() {}.type)

package com.denchikby.vkphoneparse

import com.denchikby.vkphoneparse.dao.ApiResponse
import com.denchikby.vkphoneparse.dao.Groups
import com.denchikby.vkphoneparse.dao.Users
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializer
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

object API {

    val API_BASE_URL = "https://api.vk.com/method/"

    val ACCESS_TOKEN = "e73706d6607d7b7d9c7f1976f0096c8e1f9d34d51caf37df3153647ffa568eca4f83df7148132aa7d64bf"

    val service: APIService by lazy {
        retrofit.create(APIService::class.java)
    }

    private val retrofit: Retrofit by lazy {
        Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
    }

    val gson: Gson by lazy {
        GsonBuilder()
                .registerTypeAdapter(Boolean::class.java, JsonDeserializer<Boolean>() { json, typeOfT, jsonDeserializationContext ->
                    ternary(json.asInt == 0, false, true)
                })
                .create()
    }

    interface APIService {

        @GET("groups.getById")
        fun groupsGetById(@Query("group_id") groupId: Int,
                          @Query("fields") fields: String = ""): Call<ApiResponse<List<Groups.GetById>>>

        @GET("groups.getMembers")
        fun groupsGetMembers(@Query("group_id") groupId: Int,
                             @Query("offset") offset: Int = 0): Call<ApiResponse<Groups.GetMembers>>

        @POST("users.get")
        @FormUrlEncoded
        fun usersGet(@Field("user_ids") userIds: String,
                     @Field("fields") fields: String = "",
                     @Field("access_token") accessToken: String = ACCESS_TOKEN): Call<ApiResponse<List<Users.Get>>>

    }

}

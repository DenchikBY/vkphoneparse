package com.denchikby.vkphoneparse

import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import java.util.concurrent.locks.StampedLock
import kotlin.concurrent.thread
import kotlin.properties.Delegates

object Application {

    val phoneRegex = Regex("\\+375\\d{9}")

    val outputPhones = StringBuilder()

    val lock = StampedLock()

    val threadsCount = 3

    val requestsPerSecond = 3

    var counter: Int by Delegates.observable(0) { property, old, new ->
        if (new == requestsPerSecond) {
            thread {
                lock.readLock().let {
                    Thread.sleep(1000)
                    lock.unlockRead(it)
                }
            }
        }
    }
    var totalUsersProcessed = 0

    val rootDir: String by lazy {
        var jarPath = this.javaClass.protectionDomain.codeSource.location.toURI().path
        if (jarPath.endsWith(".jar")) {
            jarPath = File(jarPath).parentFile.path
        }
        return@lazy jarPath
    }

    fun readInt(message: String, callback: (Int) -> Unit) {
        val br = BufferedReader(InputStreamReader(System.`in`))
        var result = 0
        while (result == 0) {
            print(message)
            try {
                result = Integer.parseInt(br.readLine())
                if (result > 0) {
                    callback(result)
                }
            } catch(e: NumberFormatException) {
            }
        }
    }

    fun confirmation(message: String, agree: String, disagree: String): Boolean {
        val br = BufferedReader(InputStreamReader(System.`in`))
        var result = ""
        while (!result.equals(agree) && !result.equals(disagree)) {
            print("$message [$agree/$disagree] ")
            result = br.readLine()
        }
        return result.equals(agree)
    }

    @JvmStatic fun main(args: Array<String>) {
        readInt("ID группы: ") { groupId ->
            println("Загрузка данных о группе...")
            val group = API.service.groupsGetById(groupId, "members_count").execute().body().response
            println("Группа: ${group[0].name}")
            println("Пользователей: ${group[0].members_count}")
            if (confirmation("Верно?", "да", "нет")) {
                val thPerThread = group[0].members_count / threadsCount / 1000
                val executor = Executors.newFixedThreadPool(threadsCount)
                for (i in 1..threadsCount) {
                    executor.submit(thread(start = false, name = "Поток-$i") {
                        var offset = thPerThread * (i - 1) * 1000
                        for (j in 1..thPerThread) {
                            lock.writeLock().let {
                                counter = ternary(counter == 3, 1, counter + 1)
                                lock.unlockWrite(it)
                            }
                            try {
                                val userIds = API.service.groupsGetMembers(groupId, offset).execute().body().response.users
                                if (userIds.size == 0) {
                                    break
                                } else {
                                    val users = API.service.usersGet(userIds.joinToString(","), "contacts").execute().body().response
                                    users.filter {
                                        it.mobile_phone != null && it.mobile_phone.matches(phoneRegex)
                                    }.forEach { user ->
                                        outputPhones.append("${user.first_name};${user.last_name};${user.mobile_phone}${System.lineSeparator()}")
                                    }
                                }
                                offset += 1000
                                lock.writeLock().let {
                                    totalUsersProcessed += 1000
                                    if (totalUsersProcessed > 0 && totalUsersProcessed % 10000 == 0) {
                                        println("Обработано ${totalUsersProcessed.toString()}")
                                    }
                                    lock.unlockWrite(it)
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                        println("${Thread.currentThread().name} завершен")
                    })
                }
                println("Запущен сбор данных в $threadsCount потоков...")
                executor.shutdown()
                executor.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS)
                println("Все потоки завершены!")
                File(rootDir + "phones.csv").writeText(outputPhones.toString())
                println("Завершено!")
            }
        }
    }

}

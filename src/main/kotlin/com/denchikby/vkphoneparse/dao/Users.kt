package com.denchikby.vkphoneparse.dao

object Users {

    data class Get(val uid: Int,
                   val first_name: String,
                   val last_name: String,
                   val mobile_phone: String?)

}

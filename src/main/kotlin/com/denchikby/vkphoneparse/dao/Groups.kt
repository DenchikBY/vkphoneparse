package com.denchikby.vkphoneparse.dao

object Groups {

    data class GetById(val gid: Int, val name: String, val members_count: Int)

    data class GetMembers(val count: Int, val users: List<Int>)

}

package com.denchikby.vkphoneparse.dao

data class ApiResponse<T>(val response: T)
